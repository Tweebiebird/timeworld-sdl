#pragma once

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <SDL.h>
#include <memory>
#include <string>
#include <chrono>
#include "Renderer.h"

class GameObject;

class PhysicsObject
{
public:
    GameObject* parentObject;
    PhysicsObject(int gravity, GameObject *parentObject) : gravity(gravity), parentObject(parentObject) {}
    ~PhysicsObject(){}

    bool isColliding = 0;

    void calculatePhysics();

private:
    int previousTime = SDL_GetTicks();
    int getDeltaTime()
    {
        auto currentTime = SDL_GetTicks();
        auto deltaTime = currentTime - previousTime;
        return deltaTime;
    }
    void applyVelocity();
    void applyGravity();
    float gravity = 1.0f;
    float xVelocity = 0.0f;
    float yVelocity = 0.0f;
};



class GameObject{
public:
    //For placing frame
    SDL_Rect bounding;
    std::string filename;
    std::unique_ptr<Renderer> objectRenderer;
    std::unique_ptr<PhysicsObject> objectPhysics;


    //Don't need to delete renderer as we don't own it. It gets passed in
    ~GameObject();

    //Initialization

    GameObject(std::string filename, int numFrames, int frameWidth, int frameHeight, SDL_Surface* surface, SDL_Renderer* renderer);

    //Setters

    void setLocation(int xpos, int ypos)
    {
        bounding.x = xpos;
        bounding.y = ypos;
    }
    void setSize(int width, int height) 
    {
        bounding.w = width;
        bounding.h = height;
    }

    //Rendering

    // Calls the renderers render
    void render(SDL_Rect cameralocation)
    {
        objectRenderer->render(cameralocation, bounding);
    }
    void updateFrame(int playerpos);

    //Physics
    void calculatePhysics() {
        objectPhysics->calculatePhysics();
    }
};
#endif // GAMEOBJECT_H