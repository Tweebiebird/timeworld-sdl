#pragma once
#include "Renderer.h"

enum CollisionType 
{
    TopToBottom,
    BottomToTop,
    SideToSide
};

class Wall 
{
public:
    CollisionType isColliding(SDL_Rect);

private:
    SDL_Rect bounding;
    std::string filename;
    std::unique_ptr<Renderer> objectRenderer;
};
