#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <memory>
#include <math.h>
#include <string>
#include <vector>

enum AnimationStates;

class Renderer
{
public:
    //Renders the texture using SDL_Renderer
    virtual void render(SDL_Rect cameralocation, SDL_Rect objectLocation);
    void setFrameNo(int playerpos);

    Renderer::Renderer(std::string filename, int numFrames, int frameWidth, int frameHeight, SDL_Surface * surface, SDL_Renderer * renderer);
    Renderer::~Renderer();

private:
    // Sprite sheet
    SDL_Texture* image;
    // For finding frame in sprite sheet
    int numFrames, frameWidth, frameHeight;
    // Pass in filename. Don't need to know it. std::string filename;
    // The renderer. Every object has same renderer reference
    SDL_Renderer* renderer;
    // What frame we are rendering from the sprite sheet
    int frameNo;

    //Loading
    SDL_Surface* loadSurface(std::string path, SDL_Surface& gScreenSurface);
    bool loadImage(std::string filename, SDL_Surface& gScreenSurface);

    //Helper Functions
    //This function gets the part of the sprite sheet that contains the image
    SDL_Rect getFrame();
    SDL_Rect getCameraRelativeLocation(SDL_Rect cameraPos, SDL_Rect bounding);
};