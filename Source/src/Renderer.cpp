#include "Renderer.h"

Renderer::Renderer(std::string filename, int numFrames, int frameWidth, int frameHeight, SDL_Surface * surface, SDL_Renderer * renderer) :numFrames(numFrames), frameWidth(frameWidth), frameHeight(frameHeight), renderer(renderer)
{
    //We need to surface for optimization
    loadImage(filename, *surface);
    frameNo = 0;
}

Renderer::~Renderer()
{
    SDL_DestroyTexture(image);
}

// Part of loadImage
SDL_Surface * Renderer::loadSurface(std::string path, SDL_Surface & gScreenSurface)
{
    // The final optimized image
    SDL_Surface* optimizedSurface;

    // Load image at specified path
    SDL_Surface* loadedSurface = SDL_LoadBMP(path.c_str());
    if (loadedSurface == NULL)
    {
        printf("Unable to load image %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
    }
    else
    {
        //Convert surface to screen format
        optimizedSurface = SDL_ConvertSurface(loadedSurface, gScreenSurface.format, 0);
        if (optimizedSurface == NULL)
        {
            printf("Unable to optimize image %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
        }

        // Get rid of old loaded surface
        SDL_FreeSurface(loadedSurface);
    }

    return optimizedSurface;
}

// Loads spritesheet and stores it in texture
// Later we determine which part of the sprite sheet to look at
bool Renderer::loadImage(std::string filename, SDL_Surface & gScreenSurface)
{
    //Loading success flag
    bool success = true;

    //Load stretching surface
    SDL_Surface* surface = loadSurface(filename, gScreenSurface);
    if (surface == NULL)
    {
        printf("Failed to load stretching image!\n");
        success = false;
    }
    //Get the texture and store it in object
    image = SDL_CreateTextureFromSurface(renderer, surface);
    //We don't need the surface anymore??
    SDL_FreeSurface(surface);
    return success;
}

// Renders the texture using SDL_Renderer
void Renderer::render(SDL_Rect cameralocation, SDL_Rect bounding) {
    // Apply the image stretched
    SDL_Rect stretchRect = getCameraRelativeLocation(cameralocation, bounding);
    SDL_Rect destRect = getFrame();
    // Renders a part of the sprite sheet(destRect) to a part of the screen(srcRect)
    // Should be fine to use references to local variables as they won't be out of scope till after the call to RenderCopy
    SDL_RenderCopy(renderer, image, &destRect, &stretchRect);
}

// This function gets the part of the sprite sheet that contains the image
SDL_Rect Renderer::getFrame() {
    SDL_Rect frameRect;
    int actualFrameNo = frameNo % numFrames;
    frameRect.x = actualFrameNo * frameWidth;
    frameRect.y = 0;
    frameRect.w = frameWidth;
    frameRect.h = frameHeight;
    return frameRect;
}

// Gets the location of the sprite realtive to where the camera is so we know where on the screen to render it
// Basically converts from global coordinates to screen coordinates
SDL_Rect Renderer::getCameraRelativeLocation(SDL_Rect cameraPos, SDL_Rect bounding) {
    SDL_Rect stretchRect;
    stretchRect.x = (bounding.x - cameraPos.x);
    stretchRect.y = (cameraPos.y - bounding.y);
    stretchRect.w = bounding.w;
    stretchRect.h = bounding.h;
    return stretchRect;
}

void Renderer::setFrameNo(int playerpos)
{
    frameNo = playerpos % 20;
}
