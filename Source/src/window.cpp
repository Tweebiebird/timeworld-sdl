#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <GameObject.h>

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

SDL_Surface* gScreenSurface = NULL;

SDL_Renderer* gRenderer = NULL;

std::vector<GameObject*> gameObjects;

std::string filePrefix = "Debug/";

int playerpos;

SDL_Rect cameraPosition;

GameObject* player;

void createObjects() 
{
    //Camera is Top LEft same as SDL
    cameraPosition.x = 0;
    cameraPosition.y = 0;
    cameraPosition.w = SCREEN_WIDTH;
    cameraPosition.h = SCREEN_HEIGHT;

    // Virtualization and unique_ptr gameObjects.push_back(std::make_unique<GameObject>((GameObject*)screen));
    {
        int totalframe = 920;
        GameObject*  object = new GameObject(filePrefix + "Walking.bmp", 8, 920 / 8, 193, gScreenSurface, gRenderer);
        object->setLocation(100, 0);
        object->setSize(920 / 8, 193);
        gameObjects.push_back(object);
    }
    //Render a field of things
    for (int i = 0; i < 5000; i = i + 920 / 8) 
    {
        for (int j = 0; j < 1000; j = j + 193)
        {
            int totalframe = 920;
            GameObject* object = new GameObject(filePrefix + "Walking.bmp", 8, 920 / 8, 193, gScreenSurface, gRenderer);
            object->setLocation(i, j);
            object->setSize(920 / 8, 193);
            gameObjects.push_back(object);
        }
    }
    {
        int totalframe = 920;
        player = new GameObject(filePrefix + "Walking.bmp", 8, 920 / 8, 193, gScreenSurface, gRenderer);
        player->setLocation(0, 0);
        player->setSize(920 / 8, 193);
    }
}

bool init() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        return false;
    }
    gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (gWindow == NULL)
    {
        printf("I am sad");
        return false;
    }
    gScreenSurface = SDL_GetWindowSurface(gWindow);

    //Create renderer for window
    gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
    if (gRenderer == NULL)
    {
        printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
        return false;
    }
    else
    {
        //Initialize renderer color
        SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
    }
    return true;
}

void deleteGameObjects() 
{
    for (auto &object : gameObjects) 
    {
        delete(object);
    }
    delete(player);
}

void close()
{
    SDL_DestroyWindow(gWindow);
    SDL_DestroyRenderer(gRenderer);
    deleteGameObjects();
    gWindow = NULL;
    //Quit SDL subsystems
    SDL_Quit();
}

bool isOnscreen(const GameObject &obj, SDL_Rect cameraPosition) 
{
    //Don't worry about y because things shouldn't move up and down
    int a = (cameraPosition.x + cameraPosition.w + obj.bounding.w);
    if ((obj.bounding.x < cameraPosition.x-obj.bounding.w) || (obj.bounding.x > a)) 
    {
        return false;
    }
    return true;
}

int main(int argc, char* args[])
{
    std::cout << "Hello\n";
    bool quit = false;
    SDL_Event e;
    //If initilization filas just quit otherwise consturct objects
    if (init()) 
    {
        createObjects();
    }
    else 
    {
        quit = true;
    }
    int i = 0;
    while (!quit) 
    {
        bool increment = false;
        //Clear screen
        SDL_RenderClear(gRenderer);
        while (SDL_PollEvent(&e) != 0)
        {
            if (e.type == SDL_QUIT) 
            {
                quit = true;
            }
            else if (e.type == SDL_KEYDOWN)
            {
                //Select surfaces based on key press
                switch (e.key.keysym.sym)
                {
                case SDLK_UP:
                    cameraPosition.y+=10;
                    break;
                case SDLK_DOWN:
                    cameraPosition.y-=10;
                    break;
                case SDLK_RIGHT:
                    playerpos++;
                    cameraPosition.x+=10;
                    break;
                case SDLK_LEFT:
                    playerpos--;
                    cameraPosition.x-=10;
                    break;
                default:
                    ;
                }
            }
        }

        //Physics Code
        for (auto const& object : gameObjects) {
            object->calculatePhysics();
        }

        //Render Code
        int rendered = 0;
        for (auto const& object : gameObjects) {
            if (isOnscreen(*object, cameraPosition)) 
            {
                rendered++;
                object->updateFrame(playerpos);
                object->render(cameraPosition);
            }
        }
        //std::cout << rendered << "\n";
        player->setLocation(cameraPosition.x-player->bounding.w/2 , cameraPosition.y+player->bounding.h/2);
        player->render(cameraPosition);
        //Update screen
        SDL_RenderPresent(gRenderer);
    }

    close();
    return 0;
}