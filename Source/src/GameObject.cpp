#include "GameObject.h"
#include <iostream>

//Don't need to delete renderer as we don't own it. It gets passed in


void GameObject::updateFrame(int playerpos) 
{
    objectRenderer->setFrameNo(playerpos);
}

GameObject::~GameObject()
{
    objectRenderer.reset(nullptr);
}

GameObject::GameObject(std::string filename, int numFrames, int frameWidth, int frameHeight, SDL_Surface * surface, SDL_Renderer * renderer)
{
    objectRenderer = std::make_unique<Renderer>(filename, numFrames, frameWidth, frameHeight, surface, renderer);
    objectPhysics = std::make_unique<PhysicsObject>(1, this);
    //We need to surface for optimization
}

void PhysicsObject::calculatePhysics()
{
    applyGravity();
    applyVelocity();
    previousTime= SDL_GetTicks();
}

void PhysicsObject::applyVelocity()
{
    if (xVelocity > 1) {
        xVelocity = 1;
    }
    if (yVelocity > 1) {
        yVelocity = 1;
    }
    parentObject->bounding.y += yVelocity;
    parentObject->bounding.x += xVelocity;
}

void PhysicsObject::applyGravity()
{
    auto deltaTime = getDeltaTime();
    yVelocity -= gravity*((float)deltaTime/100);
}
