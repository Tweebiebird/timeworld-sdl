#include "Wall.h"

CollisionType Wall::isColliding(SDL_Rect objectBounding)
{
	int thisTopPlane = bounding.y;
	int objectTopPlane = objectBounding.y;

	int thisBottomPlane = bounding.y + bounding.h;
	int objectBottomPlane = objectBounding.y + objectBounding.h;

	int thisLeftPlane = bounding.x;
	int objectLeftPlane = objectBounding.x;

	int thisRightPlane = bounding.x + bounding.w;
	int objectRightPlane = objectBounding.x + objectBounding.w;

	if (objectBottomPlane<thisBottomPlane && objectBottomPlane>thisTopPlane) {
		return TopToBottom;
	}
}
